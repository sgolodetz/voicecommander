#! /bin/bash -e

# Extract the Sphinx4 library.
cd libraries
./extract-sphinx4.sh
cd ..

# Build voicecommander itself.
ant

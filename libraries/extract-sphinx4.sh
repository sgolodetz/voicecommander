#! /bin/bash -e

PLATFORM=`../detect-platform.sh`

echo "[voicecommander] Extracting Sphinx4"

if [ -d sphinx4 ]
then
  echo "[voicecommander] ...Skipping (already extracted)"
  exit
else
  echo "[voicecommander] ...Extracting archive..."
  /bin/rm -fR tmp
  mkdir tmp
  cd tmp
  unzip ../setup/sphinx4/sphinx4-1.0beta6-bin.zip
  /bin/rm -fR META-INF
  mv sphinx4-1.0beta6 sphinx4

  echo "[voicecommander] ...Extracting jsapi.jar..."
  cd sphinx4/lib

  if [ $PLATFORM == "win" ]
  then
    cmd //c "jsapi.exe /S"
  else
    perl -i -lne 'unless(/^more/ ... /^fi/) { print; }' jsapi.sh
    chmod +x jsapi.sh
    ./jsapi.sh > /dev/null 2>&1
  fi

  cd ../../..
  mv tmp/sphinx4 .
  rmdir tmp

  echo "[voicecommander] ...Finished extracting Sphinx4."
fi

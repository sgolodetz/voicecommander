#! /bin/bash -e

PLATFORM=linux
UNAME=`uname`

if [ "$UNAME" == "Darwin" ]
then
  PLATFORM=mac
fi

if [ "$UNAME" == "MINGW32_NT-6.2" ]
then
  PLATFORM=win
fi

echo $PLATFORM

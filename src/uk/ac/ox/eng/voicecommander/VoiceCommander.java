/**
 * voicecommander: VoiceCommander.java
 * Copyright (c) Torr Vision Group, University of Oxford, 2015. All rights reserved.
 */

package uk.ac.ox.eng.voicecommander;

import java.io.*;
import java.net.*;

import javax.speech.EngineException;
import javax.speech.EngineStateError;
import javax.speech.recognition.GrammarException;
import javax.speech.recognition.Rule;
import javax.speech.recognition.RuleGrammar;
import javax.speech.recognition.RuleParse;

import com.sun.speech.engine.recognition.BaseRecognizer;
import com.sun.speech.engine.recognition.BaseRuleGrammar;

import edu.cmu.sphinx.frontend.util.Microphone;
import edu.cmu.sphinx.jsgf.JSGFGrammar;
import edu.cmu.sphinx.jsgf.JSGFGrammarException;
import edu.cmu.sphinx.jsgf.JSGFGrammarParseException;
import edu.cmu.sphinx.recognizer.Recognizer;
import edu.cmu.sphinx.result.Result;
import edu.cmu.sphinx.util.props.ConfigurationManager;
import edu.cmu.sphinx.util.props.PropertyException;

/**
 * \brief An instance of this class runs a server that can yield voice commands to its clients.
 */
public class VoiceCommander
{
  //#################### NESTED CLASSES ####################

  /**
   * \brief An instance of this class represents a thread that handles an individual client.
   */
  private class ClientThread extends Thread
  {
    //~~~~~~~~~~~~~~~~~~~~ PRIVATE VARIABLES ~~~~~~~~~~~~~~~~~~~~

    /** A socket representing the connection to the client. */
    private Socket m_socket;

    //~~~~~~~~~~~~~~~~~~~~ CONSTRUCTORS ~~~~~~~~~~~~~~~~~~~~

    /**
     * \brief Constructs a client thread.
     *
     * \param socket A socket representing the connection to the client.
     */
    public ClientThread(Socket socket)
    {
      m_socket = socket;
    }

    //~~~~~~~~~~~~~~~~~~~~ PUBLIC METHODS ~~~~~~~~~~~~~~~~~~~~

    @Override
    public void run()
    {
      PrintWriter writer = null;
      try
      {
        writer = new PrintWriter(new OutputStreamWriter(m_socket.getOutputStream()));

        for(;;)
        {
          synchronized(VoiceCommander.this.m_commandReady)
          {
            // Wait for a voice command.
            VoiceCommander.this.m_commandReady.wait();

            // As soon as a voice command is ready, dispatch it to the client.
            writer.println(VoiceCommander.this.m_command);
            writer.flush();
          }
        }
      }
      catch(Exception e)
      {
        System.err.println("Error: " + e.getMessage());
      }
      finally
      {
        if(writer != null) writer.close();
      }
    }
  }

  /**
   * \brief An instance of this class represents a thread that runs the voice command server.
   */
  private class ServerThread extends Thread
  {
    //~~~~~~~~~~~~~~~~~~~~ PRIVATE VARIABLES ~~~~~~~~~~~~~~~~~~~~

    /** The port on which to run the server. */
    private int m_port;

    //~~~~~~~~~~~~~~~~~~~~ CONSTRUCTORS ~~~~~~~~~~~~~~~~~~~~

    /**
     * \brief Constructs a thread that runs a server on the specified port.
     *
     * \param port The port on which to run the server.
     */
    public ServerThread(int port)
    {
      m_port = port;
    }

    //~~~~~~~~~~~~~~~~~~~~ PUBLIC METHODS ~~~~~~~~~~~~~~~~~~~~

    @Override
    public void run()
    {
      try
      {
        // Start the server.
        ServerSocket serverSocket;
        synchronized(VoiceCommander.this.m_serverReady)
        {
          System.out.print("Starting server on port " + m_port + "...");
          serverSocket = new ServerSocket(m_port);
          System.out.println("done");

          System.out.println("Listening for connections...");
          VoiceCommander.this.m_serverReady.notify();
        }

        // Listen for client connections, and launch new client threads as necessary.
        for(;;)
        {
          Socket clientSocket = serverSocket.accept();

          System.out.println("Accepted client connection");
          Thread clientThread = new ClientThread(clientSocket);
          clientThread.start();
        }
      }
      catch(Exception e)
      {
        System.err.println("Error: " + e.getMessage());
      }
    }
  }

  //#################### PRIVATE VARIABLES ####################

  /** The most recent voice command (if any). */
  private String m_command;

  /** A semaphore used to signal when a new command is ready. */
  private Object m_commandReady = new Object();

  /** The JSAPI recogniser. */
  private BaseRecognizer m_jsapiRecognizer;

  /** The grammar we're recognising. */
  private JSGFGrammar m_jsgfGrammar;

  /** The microphone. */
  private Microphone m_microphone;

  /** The base recogniser. */
  private Recognizer m_recognizer;

  /** A semaphore used to signal when the server is ready to accept client connections. */
  private Object m_serverReady = new Object();

  //#################### CONSTRUCTORS ####################

  /**
   * \brief Runs the voice commander application.
   *
   * \param grammarName The name of the grammar we want to recognise.
   * \param port        The port on which to run the voice command server.
   */
  public VoiceCommander(String grammarName, int port) throws Exception
  {
    System.out.println("Voice Commander");
    System.out.println("===============\n");

    // Load the configuration from the XML file.
    System.out.print("Loading configuration...");
    ConfigurationManager cm = new ConfigurationManager("resources/voicecommander.config.xml");
    m_jsgfGrammar = (JSGFGrammar)cm.lookup("jsgfGrammar");
    m_microphone = (Microphone)cm.lookup("microphone");
    m_recognizer = (Recognizer)cm.lookup("recognizer");
    System.out.println("done");

    // Set up the voice recogniser.
    System.out.print("Setting up recogniser...");
    m_jsapiRecognizer = new BaseRecognizer(m_jsgfGrammar.getGrammarManager());
    m_jsapiRecognizer.allocate();
    m_recognizer.allocate();
    System.out.println("done");

    // Load the grammar we want to recognise.
    System.out.print("Loading grammar...");
    m_jsgfGrammar.loadJSGF(grammarName);
    System.out.println("done");

    // Start the microphone.
    System.out.print("Starting microphone...");
    if(!m_microphone.startRecording()) throw new Exception("Could not start microphone");
    System.out.println("done");

    // Start the server.
    synchronized(m_serverReady)
    {
      Thread serverThread = new ServerThread(port);
      serverThread.start();
      m_serverReady.wait();
    }
    System.out.println();

    // Print some sample sentences from the grammar so that the user knows what to say.
    printSampleSentences(grammarName);

    // Run the voice recogniser.
    try
    {
      runRecogniser();
    }
    catch(Exception e)
    {
      System.err.println("Error: " + e.getMessage());
    }

    // Shut down the voice recogniser and exit.
    System.out.println("Shutting down...");
    m_recognizer.deallocate();
    System.exit(0);
  }

  //#################### PRIVATE METHODS ####################

  /**
   * \brief Checks whether or not the rule specified by the user has an 'exit' tag.
   *
   * \param ruleParse A parse of the rule.
   * \return          true, if the rule has an 'exit' tag, or false otherwise.
   */
  private boolean hasExitTag(RuleParse ruleParse)
  {
    String[] tags = ruleParse.getTags();
    if(tags == null) return false;

    for(int i = 0; i < tags.length; ++i)
    {
      if(tags[i].trim().equals("exit")) return true;
    }

    return false;
  }

  /**
   * Prints out a set of random sample sentences for the grammar we're using.
   *
   * \param grammarName The name of the grammar we're using.
   */
  private void printSampleSentences(String grammarName)
  {
    System.out.println(" ====== " + grammarName + " ======");
    System.out.println("Speak one of: \n");
    m_jsgfGrammar.dumpRandomSentences(200);
    System.out.println(" ============================\n");
  }

  /**
   * \brief Runs the voice recogniser.
   *
   * The recogniser iteratively recognises new commands until an 'exit' command is specified.
   *
   * \throws GrammarException If an error is the JSGF grammar is encountered.
   */
  private void runRecogniser() throws GrammarException
  {
    boolean done = false;
    while(!done)
    {
      Result result = m_recognizer.recognize();
      synchronized(m_commandReady)
      {
        m_command = result.getBestFinalResultNoFiller();
        RuleGrammar ruleGrammar = new BaseRuleGrammar(m_jsapiRecognizer, m_jsgfGrammar.getRuleGrammar());
        RuleParse ruleParse = ruleGrammar.parse(m_command, null);
        if(ruleParse != null)
        {
          System.out.println("\n  Recognised: " + m_command + '\n');
          m_commandReady.notifyAll();
          //done = hasExitTag(ruleParse);
        }
      }
    }
  }

  //#################### PUBLIC STATIC METHODS ####################

  /**
   * \brief Runs the program.
   *
   * \param args The command-line arguments.
   */
  public static void main(String[] args)
  {
    try
    {
      new VoiceCommander("spaint", 23984);
    }
    catch(Exception e)
    {
      System.err.println("Error: " + e);
    }
  }
}

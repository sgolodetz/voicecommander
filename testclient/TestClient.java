/**
 * voicecommander: TestClient.java
 * Copyright (c) Torr Vision Group, University of Oxford, 2015. All rights reserved.
 */

import java.io.*;
import java.net.*;

public class TestClient
{
  public static void main(String[] args)
  {
    Socket s = null;
    BufferedReader reader = null;
    try
    {
      s = new Socket("localhost", 23984);
      reader = new BufferedReader(new InputStreamReader(s.getInputStream()));

      String line;
      while((line = reader.readLine()) != null)
      {
        System.out.println(line);
      }
    }
    catch(Exception e)
    {
      System.err.println("Error: " + e.getMessage());
    }
    finally
    {
      try
      {
        if(reader != null) reader.close();
        if(s != null) s.close();
      }
      catch(IOException e) {}
    }
  }
}
